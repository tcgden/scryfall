<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 24/11/2020
 * Description:
 */

use App\Config\Controllers;
use App\Config\Routes;
use App\Config\Settings;
use App\Sockets\Commands;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use System\Sockets\Server;
use System\Startup\Startup;

require __DIR__ . '/../vendor/autoload.php';

try {
    $startup = new Startup(new Settings(), new Controllers(), new Routes());
    $app = $startup->execute();
    $server = IoServer::factory(new HttpServer(new WsServer(new Server($app, new Commands($app)))), 8080);
    $server->run();
} catch (DomainException $e) {
}