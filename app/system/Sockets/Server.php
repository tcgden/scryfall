<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 20/11/2020
 * Description:
 */

namespace System\Sockets;

use Exception;
use Ratchet\ConnectionInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;
use Ratchet\WebSocket\MessageComponentInterface;
use SplObjectStorage;

class Server implements MessageComponentInterface {


    /**
     * @var SplObjectStorage
     */
    private SplObjectStorage $clients;
    /**
     * @var ACommands
     */
    private ACommands $commands;

    /**
     * Server constructor.
     * @param ACommands $commands
     */
    public function __construct(ACommands $commands)
    {
        $this->clients = new SplObjectStorage();
        $this->commands = $commands;
    }

    public function onOpen(ConnectionInterface $conn): void
    {
        $this->clients->attach($conn);
    }

    public function onClose(ConnectionInterface $conn): void
    {
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, Exception $e): void
    {
        echo 'error';
    }

    public function onMessage(ConnectionInterface $conn, MessageInterface $msg): void
    {
        $this->commands->setConnections($conn, $this->clients);
        $this->commands->resolve($msg);
    }
}