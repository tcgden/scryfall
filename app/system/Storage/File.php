<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 14/04/2020
 * Description:
 */

namespace System\Storage;

use JsonException;
use System\Exceptions\EmptyException;
use System\Exceptions\GenericException;

/**
 * Class File
 * @package System\Storage
 */
class File
{
    /**
     * @var string
     */
    private string $path;

    /**
     * @var int
     */
    private int $expire;

    /**
     * File constructor.
     * @param string $path path to the folder
     * @param int $expire time to expire
     */
    public function __construct(string $path, int $expire)
    {
        $this->path = $path;
        $this->expire = $expire;
    }

    /**
     * @param string $scope
     * @param string $key
     * @param array $value
     * @param bool $noExpireDate set to true to invalid the expire date
     * @throws GenericException
     */
    public function set(string $scope, string $key, array $value, $noExpireDate = false): void
    {
        try {
            if (!is_dir($this->path. '/' . $scope)) {
                /** @noinspection MkdirRaceConditionInspection */
                mkdir($this->path. '/' . $scope, 0777, true);
            }
            $expire = $this->expire;
            if ($noExpireDate === true) {
                $expire = false;
            }
            file_put_contents($this->path . '/' . $scope . '/' . $key, json_encode([
                'data' => $value,
                'expire' => $expire,
                'created' => time()
            ], JSON_THROW_ON_ERROR));
        } catch (JsonException $e) {
            throw new GenericException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param string $scope
     * @param string $key
     * @param bool $toDelete
     * @return array
     * @throws EmptyException
     */
    public function get(string $scope, string $key, $toDelete = false): array
    {
        try {
            if (!is_file($this->path. '/' . $scope . '/' . $key)) {
                throw new EmptyException('storage item does not exist yet!');
            }
            $content = json_decode(file_get_contents($this->path . '/' . $scope . '/' . $key), true, 512, JSON_THROW_ON_ERROR);
            if ($content['expire'] !== false) {
                $expire = (int) $content['expire'];
                $created = (int) $content['created'];
                if ((time() - $created) > $expire) {
                    // expired
                    unlink($this->path. '/' . $scope . '/' . $key);
                    throw new EmptyException('storage item expired');
                }
            }
            if ($toDelete) {
                unlink($this->path. '/' . $scope . '/' . $key);
            }
            return $content['data'];
        } catch (JsonException $e) {
            throw new EmptyException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param string $scope
     * @param string $key
     */
    public function remove(string $scope, string $key): void
    {
        if (!is_dir($this->path. '/' . $scope)) {
            /** @noinspection MkdirRaceConditionInspection */
            mkdir($this->path. '/' . $scope, 0777, true);
        }
        unlink($this->path. '/' . $scope . '/' . $key);
    }

    /**
     * @param string $additionalPath
     * @return string
     */
    public function getPath($additionalPath = ''): string
    {
        return $this->path . $additionalPath;
    }

    /**
     * @param string $scope
     * @param string $key
     * @return bool
     */
    public function has(string $scope, string $key): ?bool
    {
        return is_file($this->path. '/' . $scope . '/' . $key);
    }
}