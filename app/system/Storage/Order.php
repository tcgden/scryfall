<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 10/06/2020
 * Description:
 */

namespace System\Storage;


class Order
{
    /**
     * @param string $path
     * @return array
     */
    protected function getOrder($path): array
    {
        if (is_file($path . '/.order')) {
            return json_decode(file_get_contents($path . '/.order'), true);
        }
        return [];
    }

    /**
     * @param string $path
     * @param array $content
     */
    protected function setOrder($path, array $content): void
    {
        file_put_contents( $path . '/.order', json_encode($content));
    }

    protected function removeOrder($path) {
        if (is_file($path . '/.order')) {
            unlink($path . '/.order');
        }
    }
}