<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 11/04/2020
 * Description:
 */

namespace System\Middleware;


use DateTime;
use Exception;
use Firebase\JWT\JWT;
use InvalidArgumentException;
use JsonException;
use Slim\Container;
use Slim\Exception\ContainerException;
use Slim\Http\Request;
use Slim\Http\Response;
use stdClass;
use System\Exceptions\TokenException;
use System\Storage\Session;
use System\Utils\Crypto;
use UnexpectedValueException;

class TokenAuth
{

    /**
     * @var Container
     */
    private $container;

    /**
     * TokenAuth constructor.
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->container = $container;
    }

    /**
     * @param array $user
     * @param Request $request
     * @return string
     * @throws TokenException
     */
    public function encode(array $user, Request $request): string
    {
        try {
            $now = new DateTime();
            $future = new DateTime('now + 1week');
            $secret = $this->getSettings('secret');
            $algorithm = $this->getSettings('algorithm');
            $payload = [
                'jti' => Crypto::encrypt(json_encode($user, JSON_THROW_ON_ERROR), $secret),
                'iat' => $now->getTimeStamp(),
                'exp' => $future->getTimeStamp()
            ];
            $token = JWT::encode($payload, $secret, $algorithm);
            $this->getSession()->set($token, $request, $user['user_id']);
            return $token;
        } catch (Exception $e) {
            throw new TokenException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $next)
    {
        try {
            try {
                $token = current($request->getHeader('HTTP_X_TOKEN'));
                $secret = $this->getSettings('secret');
                $algorithm = $this->getSettings('algorithm');
                $data = JWT::decode($token, $secret, [$algorithm]);
                if ($data === null) {
                    throw new TokenException('token must be refreshed');
                }
                $user  = json_decode(Crypto::decrypt($data->jti, $secret), false, 512, JSON_THROW_ON_ERROR);
                if (!$this->getSession()->has($token, $request, $user->user_id)) {
                    throw new TokenException('token must be refreshed');
                }
                $request = $request->withAttribute('user', $user);
                return $next($request, $response);
            } catch (UnexpectedValueException $e) {
                throw new TokenException($e->getMessage());
            }
        } catch (TokenException | JsonException | Exception $e) {
            return $response->withJson([
                'code' => $e->getErrorCode(),
                'error' => $e->getErrorMessage()
            ], 401);
        }
    }

    /**
     * @param Request $request
     * @return stdClass
     */
    public static function getUser(Request $request): stdClass
    {
        return $request->getAttribute('user');
    }

    /**
     * @param Request $request
     * @return bool
     * @throws TokenException
     */
    public function destroy(Request $request): bool
    {
        $user = $request->getAttribute('user');
        return $this->getSession()->remove($request, $user->user_id);
    }

    /**
     * @param string $index
     * @return string
     * @throws ContainerException
     * @throws InvalidArgumentException
     */
    public function getSettings(string $index): string
    {
        return $this->container->get('settings')['jwt'][$index];
    }

    /**
     * @return Session
     * @throws TokenException
     */
    public function getSession(): Session
    {
        try {
            return $this->container->get('Session');
        } catch (ContainerException | InvalidArgumentException $e) {
            throw new TokenException($e->getMessage());
        }
    }

}