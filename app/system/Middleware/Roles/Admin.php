<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 12/04/2020
 * Description:
 */

namespace System\Middleware\Roles;


class Admin extends ARole
{
    static public $id = 2;

    public function checkPermission($userRoleName, $roleName): bool
    {
        switch ($userRoleName) {
            case God::getName():
            case self::getName():
                return true;
            default:
                return false;
        }
    }

    public static function getName(): string
    {
        return 'Admin';
    }
}