<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 12/04/2020
 * Description:
 */

namespace System\Middleware\Roles;


use Slim\Http\Request;
use Slim\Http\Response;

interface IRole
{
    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     * @return mixed
     */
    public function __invoke(Request $request, Response $response, $next);
}