<?php

namespace System\Middleware;

use Slim\Http\Request;

class APIKey {

	protected $apiKey;

	public function __construct($apiKey) {
		$this->apiKey = $apiKey;
	}

	public function __invoke(Request $request, $response, $next) {

	    if ($request->hasHeader('HTTP_API_KEY') && $request->getHeaderLine('HTTP_API_KEY') === $this->apiKey) {
			$response = $next($request, $response);

		} else {
			$response = $response->withJson([
				'error' => [
					'message' => 'Unauthorized'
				]
			], 401);
		}

		return $response;

	}

}