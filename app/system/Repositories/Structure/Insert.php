<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 14/05/2021
 * Description:
 */

namespace System\Repositories\Structure;


use JsonException;
use PDO;

class Insert extends AType
{

    /**
     * @var array
     */
    protected array $insertValues = [];

    /**
     * Insert constructor.
     * @param array $data
     * @param array $structure
     */
    public function __construct(array $data, array $structure)
    {

        foreach($structure as $field => $type) {
            if (isset($data[$field])) {

                try {
                    $pdoValue = $data[$field];
                    $pdoType = PDO::PARAM_STR;

                    if ($type === 'json' || ( is_array($pdoValue) && strpos($type,'varchar')>-1 ) ) {
                        $pdoValue = json_encode($pdoValue, JSON_THROW_ON_ERROR);
                    } else if ($type === 'int') {
                        $pdoType = PDO::PARAM_INT;
                    }

                    $this->fieldNames[] = $field;
                    $this->insertValues[] = ':' . $field;
                    $this->bindValues[':' . $field] = [
                        'type' => $pdoType,
                        'value' => $pdoValue
                    ];
                } catch (JsonException $e) {}
            }
        }
    }

    /**
     * @return array
     */
    public function getInsertValues(): array
    {
        return $this->insertValues;
    }

}