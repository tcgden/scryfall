<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 14/05/2021
 * Description:
 */

namespace System\Repositories\Structure;

use System\Exceptions\EmptyException;
use System\Exceptions\GenericException;
use System\Storage\Database;
use System\Storage\File;

class Table
{
    /**
     * @var array
     */
    private array $structure;

    /**
     * Table constructor.
     * @param File $cache
     * @param Database $database
     * @param string $tableName
     * @throws GenericException
     */
    public function __construct(File $cache, Database $database, string $tableName)
    {
        try {
            $cachedStructure = $cache->get('structure', $tableName);
            $this->structure = $cachedStructure['fields'];
        } catch (EmptyException $e) {
            $database->read('DESCRIBE ' . $tableName);
            $tableStructure = [
                'structure' => $database->resultSet(),
                'fields' => []
            ];
            foreach($tableStructure['structure'] as $column) {
                $tableStructure['fields'][$column['Field']] = $column['Type'];
            }
            $cache->set('structure', $tableName, $tableStructure);
            $this->structure = $tableStructure['fields'];
        }
    }

    /**
     * @param array $data
     * @return Select
     */
    public function getSelectFilter(array $data): Select
    {
        return new Select($data, $this->structure);
    }

    /**
     * @param array $data
     * @return Insert
     */
    public function getInsertFilter(array $data): Insert
    {
        return new Insert($data, $this->structure);
    }

    /**
     * @param array $data
     * @return Update
     */
    public function getUpdateFilter(array $data): Update
    {
        return new Update($data, $this->structure);
    }
}