<?php /** @noinspection SqlNoDataSourceInspection */

/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 11/04/2020
 * Description:
 */

namespace System\Repositories;


use Exception;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use System\Exceptions\DeleteException;
use System\Exceptions\DuplicateException;
use System\Exceptions\EmptyException;
use System\Exceptions\GenericException;
use System\Exceptions\MissingException;
use System\Repositories\Structure\Table;
use System\Storage\Database;
use System\Storage\File;

abstract class ARepository
{
    /**
     * @var Database
     */
    private $database;

    /**
     * @var array
     */
    private $structure = false;

    /**
     * @var File
     */
    private $cache;

    /**
     * @var Logger
     */
    private $logs;

    /**
     * ARepository constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->database = $container->get('Database');
        $this->cache = $container->get('Storage');
        $this->logs = $container->get('Logger');
    }

    /**
     * @return Database
     */
    public function getDatabase(): Database
    {
        return $this->database;
    }

    /**
     * @return string
     */
    abstract public function getTableName(): string;

    /**
     * @return string
     */
    abstract public function getUniqueIdentifierName(): string;

    /**
     * @param Request $request
     * @return string
     * @throws GenericException
     */
    public function deletePermission(Request $request): string
    {
        $showDeleted = ' 1 ';
        $tableName = $this->getTableName();
        $selector = $this->getTableStructure()->getSelectFilter(['deleted' => true, 'isActive' => true]);
        if (count($selector->getFieldNames())) {
            if (($user = $request->getAttribute('user', false)) && !property_exists($user->role, 'God')) {
                $showDeleted = ' ' . $tableName . '.deleted is null ';
            } else if ($request->getAttribute('user', false) === false) {
                $showDeleted = ' ' . $tableName . '.deleted is null AND ' . $tableName . '.isActive = 1 ';
            }
        }
        return $showDeleted;
    }

    /**
     * @param Request $request
     * @param array $query
     * @throws GenericException
     */
    private function constructFilters(Request $request, array $query): void
    {
        $queryParams = $request->getQueryParams();
        $tableName = $this->getTableName();
        $selectFilter = $this->getTableStructure()->getSelectFilter(array_merge($queryParams, $query));
        $showDeleted = $this->deletePermission($request);
        if (count($selectFilter->getFieldNames())) {
            $where = 'WHERE ' . implode(' AND ', $selectFilter->getFieldNames()) . ' AND ' . $showDeleted;
            $this->getDatabase()->bindArray($selectFilter->getBindValues());
        } else {
            $where = 'WHERE ' . $showDeleted;
        }
        $limit = $group = $order = '';
        if (isset($queryParams['limit'])) {
            $limit = ' LIMIT ' . $queryParams['limit'];
        }
        if (isset($queryParams['group'])) {
            $group = ' GROUP BY ' . $queryParams['group'];
        }
        if (isset($queryParams['order'])) {
            $order = ' ORDER BY ' . $queryParams['order'];
        }
        $sql = 'SELECT * FROM ' . $tableName . ' ' . $where . ' ' . $group . ' ' . $order . ' ' . $limit;
        $this->getDatabase()->read($sql);
    }

    /**
     * @param Request $request
     * @param array $query
     * @return array
     * @throws GenericException
     */
    public function getFilteredCollection(Request $request, array $query = []): array
    {
        $this->constructFilters($request, $query);
        return $this->getDatabase()->resultSet();
    }

    /**
     * @param Request $request
     * @param array $query
     * @return array
     * @throws EmptyException
     * @throws GenericException
     */
    public function getFilteredItem(Request $request, array $query = []): array
    {
        $this->constructFilters($request, $query);
        return $this->getDatabase()->single();
    }

    /**
     * @param Request $request
     * @param string|integer $itemId
     * @return array
     * @throws EmptyException
     * @throws GenericException
     */
    public function getFilteredItemById(Request $request, $itemId): array
    {
        $this->constructFilters($request, [$this->getTableName() . '_id' => $itemId]);
        return $this->getDatabase()->single();
    }

    /**
     * @param Request $request
     * @param string $uuid
     * @return array
     * @throws EmptyException
     * @throws GenericException
     */
    public function getFilteredItemByUUID(Request $request, string $uuid): array
    {
        $this->constructFilters($request, [$this->getUniqueIdentifierName() => $uuid]);
        return $this->getDatabase()->single();
    }

    /**
     * @param $data
     * @return int
     * @throws DuplicateException
     * @throws GenericException
     * @throws MissingException
     */
    public function storeItem($data): int
    {
        try {
            $tableName = $this->getTableName();
            $tableStructure = $this->getTableStructure()->getInsertFilter($data);
            /** @noinspection SqlNoDataSourceInspection */
            $sql = "INSERT INTO $tableName (". implode(',', $tableStructure->getFieldNames()) . ') VALUES('. implode(',', $tableStructure->getInsertValues()) . ')';
            $this->logs->debug($sql);
            $this->database->write($sql);
            $this->getDatabase()->bindArray($tableStructure->getBindValues());
            $this->getDatabase()->execute();
            return $this->getDatabase()->lastInsertId();
        } catch (Exception $e) {
            if ((int) $e->getCode() === 23000) {
                throw new DuplicateException($e->getMessage(), $e->getCode());
            }
            if ($e->getCode() === 'HY000') {
                $missingException = new MissingException();
                $missingException->setMessage($e->getMessage());
                throw $missingException;
            }
            throw new GenericException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param int $itemId
     * @param array $data
     * @return bool
     * @throws DuplicateException
     * @throws GenericException
     */
    public function modifyItem(int $itemId, array $data): bool
    {
        try {
            $tableName = $this->getTableName();
            $data['modified'] = date('Y-m-d H:i:s');
            $tableStructure = $this->getTableStructure()->getUpdateFilter($data);
            $this->getDatabase()->write("UPDATE $tableName SET " . implode(',', $tableStructure->getFieldNames()) . ' WHERE ' . $tableName . "_id = $itemId");
            $this->getDatabase()->bindArray($tableStructure->getBindValues());
            $this->getDatabase()->execute();
        } catch (Exception $e) {
            if ((int) $e->getCode() === 23000) {
                throw new DuplicateException($e->getMessage(), $e->getCode());
            }
            throw new GenericException($e->getMessage());
        }
        return $this->getDatabase()->lastInsertId();
    }

    /**
     * @param Request $request
     * @param $itemId
     * @return bool
     * @throws DeleteException
     * @throws EmptyException
     */
    public function deleteItem(Request $request, $itemId): bool
    {
        try {
            $item = $this->getFilteredItemById($request, $itemId);
            if ($item['deleted'] === false) {
                return $this->modifyItem($itemId, ['deleted' => date('Y-m-d H:i:s')]);
            }
            return $this->modifyItem($itemId, ['deleted' => null]);
        } catch (DuplicateException | GenericException $e) {
            throw new DeleteException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @return int
     * @throws EmptyException
     * @throws GenericException
     */
    public function countActives(): int
    {
        $tableName = $this->getTableName();
        $uniqueIdentifierName = $this->getUniqueIdentifierName();
        $this->getDatabase()->read("SELECT count(*) AS amount FROM $tableName WHERE $uniqueIdentifierName IS NOT NULL");
        $result = $this->getDatabase()->single();
        return (int) $result['amount'];
    }

    /**
     * @return Table
     * @throws GenericException
     */
    private function getTableStructure(): Table
    {
        if (!$this->structure instanceof Table) {
            $this->structure =  new Table($this->cache, $this->database, $this->getTableName());
        }
        return $this->structure;
    }

}