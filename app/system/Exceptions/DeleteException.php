<?php

/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 10/04/2020
 * Description:
 */

namespace System\Exceptions;





class DeleteException extends AException
{
    public function getErrorCode(): int
    {
        return 6;
    }

    public function setMessage($message): void
    {
        $this->message = $message;
    }
}