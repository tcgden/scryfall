<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 14/04/2020
 * Description:
 */

namespace System\Exceptions;




class GenericException extends AException
{

    protected $message = 'Something wen\'t wrong! Try again later';



    public function getErrorCode(): int
    {
        return 3;
    }

    /**
     * @param $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }
}