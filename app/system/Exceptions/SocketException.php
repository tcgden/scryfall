<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 15/12/2020
 * Description:
 */

namespace System\Exceptions;

/**
 * Class SocketException
 * @package System\Exceptions
 */
class SocketException extends AException
{

    public function getErrorCode(): int
    {
        return 666;
    }

    public function setMessage($message): void
    {
        $this->message = $message;
    }
}