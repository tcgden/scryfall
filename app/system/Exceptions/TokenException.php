<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 15/04/2020
 * Description:
 */

namespace System\Exceptions;




class TokenException extends AException
{
    public function getErrorCode(): int
    {
        return 5;
    }

    public function setMessage($message): void
    {
        $this->message = $message;
    }
}