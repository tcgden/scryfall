<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 15/12/2020
 * Description:
 */

namespace System\Exceptions;


class SocketMessageErrorException extends AException
{

    public function getErrorCode(): int
    {
        return 999;
    }

    public function setMessage($message): void
    {
        $this->message = $message;
    }
}