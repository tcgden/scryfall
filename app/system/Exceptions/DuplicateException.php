<?php

/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 10/04/2020
 * Description:
 */

namespace System\Exceptions;





class DuplicateException extends AException
{

    public function getErrorMessage(): string
    {
        preg_match('/key \'(.*)\'/m', $this->getMessage(), $matches);
        $field = end($matches);
        return 'There is already a resource with that ' . $field;
    }

    public function getErrorCode(): int
    {
        return 1;
    }

    public function setMessage($message): void
    {
        // TODO: Implement setMessage() method.
    }
}