<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 10/04/2020
 * Description:
 */

namespace System\Controllers;


use Slim\Http\Request;
use Slim\Http\Response;

interface IController
{
    public function list(Request $request, Response $response);
    public function get(Request $request, Response $response, array $args);
    public function post(Request $request, Response $response, array $args);
    public function put(Request $request, Response $response, array $args);
    public function delete(Request $request, Response $response, array $args);
}