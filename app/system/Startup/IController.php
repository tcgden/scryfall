<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 09/11/2020
 * Description:
 */

namespace System\Startup;


use Slim\Container;

interface IController
{
    public function export(Container $container): void;
}