<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 09/11/2020
 * Description:
 */

namespace System\Startup;

use Awurth\SlimValidation\Validator;
use InvalidArgumentException;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Slim\App;
use Slim\Container;
use System\Exceptions\GenericException;
use System\Middleware\APIKey;
use System\Middleware\Roles\Admin;
use System\Middleware\Roles\Editor;
use System\Middleware\Roles\God;
use System\Middleware\Roles\User;
use System\Middleware\TokenAuth;
use System\Storage\Database;
use System\Storage\File;
use System\Storage\Media;
use System\Storage\Redis;
use System\Storage\Session;
use Throwable;

/**
 * Class Startup
 * @package System\Startup
 */
class Startup
{

    /**
     * @var ISettings
     */
    private ISettings $settings;

    /**
     * @var IController
     */
    private IController $controller;

    /**
     * @var IRouter
     */
    private IRouter $router;

    public function __construct(ISettings $settings, IController $controller, IRouter $router)
    {
        $this->settings = $settings;
        $this->controller = $controller;
        $this->router = $router;
    }

    /**
     * @return App
     * @throws GenericException
     */
    public function execute(): App
    {
        try {
            $app = new App(['settings' => $this->settings->export()]);
            /** @var Container $container */
            $container = $app->getContainer();
            $this->controller->export($container);
            $this->router->export($app);
            $container['APIKey'] = static function() {
                return new APIKey(getenv('API_KEY'));
            };
            $container['OAuth'] = static function(Container $container) {
                return new TokenAuth($container);
            };
            $container['GodRole'] = static function(Container $container) {
                return new God($container);
            };
            $container['AdminRole'] = static function(Container $container) {
                return new Admin($container);
            };
            $container['EditorRole'] = static function(Container $container) {
                return new Editor($container);
            };
            $container['UserRole'] = static function(Container $container) {
                return new User($container);
            };
            $container['Database'] = static function(Container $container) {
                $settings = $container->get('settings');
                return new Database(
                    $settings['mysql']['database']
                );
            };
            $container['Logger'] = static function(Container $container) {
                $settings = $container->get('settings');
                $logger = new Logger($settings['mysql']['database']);
                $logger->pushProcessor(new UidProcessor());
                $logger->pushHandler(new StreamHandler($settings['logger']['path'], $settings['logger']['level']));
                return $logger;
            };
            $container['Redis'] = static function() {
                return new Redis(
                    'tcp',
                    6379
                );
            };
            $container['Storage'] = static function() {
                return new File(
                    'var/storage',
                    432000
                );
            };
            $container['Session'] = static function(Container $container) {
                return new Session($container);
            };
            $container['Media'] = static function(Container $container) {
                return new Media($container);
            };
            $container['Validator'] = static function () {
                return new Validator(false);
            };
            $app->run();
            return $app;
        } catch (InvalidArgumentException | Throwable $e) {
            throw new GenericException('slim app not created! reason:' . $e->getMessage());
        }
    }
}