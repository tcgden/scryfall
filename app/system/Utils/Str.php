<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 06/01/2021
 * Description:
 */

namespace System\Utils;

use RuntimeException;
use Transliterator;

class Str
{
    /**
     * Will hold an instance of Transliterator
     * for removing accents from characters.
     * Same instance for all instances of this class is fine.
     */
    private static $accent_transliterator;
    private $string;

    public function __construct(string $string)
    {
        $this->string = $string;
    }

    public function __toString()
    {
        return $this->string;
    }

    public function cleanForUrlPath(): self
    {
        $path = '';

        // Loop through path sections (separated by `/`)
        // and slugify each section.
        foreach (explode('/', $this->string) as $section) {
            $section = (new static($section))->slugify()->__toString();
            if ($section !== '') {
                $path .= "/$section";
            }
        }

        // Save the cleaned path
        $this->string = "$path/";

        return $this;
    }

    public function cleanUpSlugDashes(): self
    {
        // Remove extra dashes
        $this->string = preg_replace('/--+/', '-', $this->string);

        // Remove leading and trailing dashes
        $this->string = trim($this->string, '-');

        return $this;
    }

    /**
     * Replace symbols with word replacements.
     * Eg, `&` becomes ` and `.
     */
    public function convertSymbolsToWords(): self
    {
        $this->string = strtr($this->string, [
            '@' => ' at ',
            '%' => ' percent ',
            '&' => ' and ',
        ]);

        return $this;
    }

    public static function getSpacerCharacters(
        array $with = [],
        array $without = []
    ): array {
        return array_unique(array_diff(array_merge([
            ' ', // space
            '…', // ellipsis
            '–', // en dash
            '—', // em dash
            '/', // slash
            '\\', // backslash
            ':', // colon
            ';', // semi-colon
            '.', // period
            '+', // plus sign
            '#', // pound sign
            '~', // tilde
            '_', // underscore
            '|', // pipe
        ], array_values($with)), array_values($without)));
    }

    public function lower(): self
    {
        $this->string = strtolower($this->string);

        return $this;
    }

    /**
     * Replaces all accented characters
     * with similar ASCII characters.
     */
    public function removeAccents(): self
    {
        // If no accented characters are found,
        // return the given string as-is.
        if (!preg_match('/[\x80-\xff]/', $this->string)) {
            return $this;
        }

        // Instantiate Transliterator if we haven't already
        if (!isset(self::$accent_transliterator)) {
            self::$accent_transliterator = Transliterator::create(
                'Any-Latin; Latin-ASCII;'
            );

            if (self::$accent_transliterator === null) {
                // @codeCoverageIgnoreStart
                throw new RuntimeException(
                    'Could not create a transliterator'
                );
                // @codeCoverageIgnoreEnd
            }
        }

        // Save transliterated string
        $this->string = (self::$accent_transliterator)->transliterate(
            $this->string
        );

        return $this;
    }

    public function replace($search, $replace)
    {
        $this->string = str_replace($search, $replace, $this->string);

        return $this;
    }

    public function replaceRegex($pattern, $replacement): self
    {
        $this->string = preg_replace($pattern, $replacement, $this->string);

        return $this;
    }

    /**
     * @param int $length number of bytes to shorten the string to
     */
    public function shorten(int $length): self
    {
        // If the string is already `$length` or shorter,
        // return it as-is.
        if (strlen($this->string) <= $length) {
            return $this;
        }

        // Shorten by 2 additional characters
        // to account for the three periods that are appended.
        // Only need to shorten by 2
        // as there's always at least one character (space) removed
        // when the last word is popped off of the array.
        $length -= 2;

        // Shorten the string to `$length` and split into words
        $words = explode(' ', substr($this->string, 0, $length));

        // Discard the last word as it's a partial word,
        // or empty if the last character happened to be a space.
        // If there's only one word,
        // then it was longer than `$length`
        // and the truncated version should be returned.
        if (count($words) > 1) {
            array_pop($words);
        }

        // Save the shortened string with "..." appended
        $this->string = rtrim(implode(' ', $words), ':').'...';

        return $this;
    }

    public function slugify(): self
    {
        // If the string is already a slug
        if (preg_match('/^[a-z0-9\\-]+$/', $this->string)) {
            return $this;
        }

        // - Normalize accents
        // - Normalize symbols
        // - Lowercase
        // - Replace space characters with dashes
        // - Remove non-slug characters
        // - Clean up leading, trailing, and consecutive dashes
        return $this
            ->removeAccents()
            ->convertSymbolsToWords()
            ->lower()
            ->spacersToDashes()
            ->replaceRegex('/([^a-z0-9\\-]+)/', '')
            ->cleanUpSlugDashes();
    }

    public function spacersToDashes(): self
    {
        return $this->replace(static::getSpacerCharacters(), '-');
    }
}