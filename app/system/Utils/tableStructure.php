<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 07/05/2021
 * Description:
 */

namespace System\Utils;


class tableStructure
{
    /**
     * @var array
     */
    private $fields;

    /**
     * @var array
     */
    private $queryParams;

    /**
     * @var array
     */
    private $insertStatementFields;

    /**
     * @var array
     */
    private $updateStatementFields;

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     */
    public function setFields(array $fields): void
    {
        $this->fields = $fields;
    }

    /**
     * @return array
     */
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    /**
     * @param array $queryParams
     */
    public function setQueryParams(array $queryParams): void
    {
        $this->queryParams = $queryParams;
    }

    /**
     * @return array
     */
    public function getInsertStatementFields(): array
    {
        return $this->insertStatementFields;
    }

    /**
     * @param array $insertStatementFields
     */
    public function setInsertStatementFields(array $insertStatementFields): void
    {
        $this->insertStatementFields = $insertStatementFields;
    }

    /**
     * @return array
     */
    public function getUpdateStatementFields(): array
    {
        return $this->updateStatementFields;
    }

    /**
     * @param array $updateStatementFields
     */
    public function setUpdateStatementFields(array $updateStatementFields): void
    {
        $this->updateStatementFields = $updateStatementFields;
    }


}