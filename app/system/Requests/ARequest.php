<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 28/05/2020
 * Description:
 */

namespace System\Requests;

use Monolog\Logger;
use Psr\Container\ContainerInterface;
use System\Exceptions\SocketException;
use System\Exceptions\SocketMessageErrorException;
use System\Sockets\Client;

/**
 * Class ARequest
 * @package System\Requests
 */
abstract class ARequest
{

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var ARequest[]
     */
    public static array $instance = [];


    /**
     * ARequest constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->logger = $container->get('Logger');
    }

    /**
     * @param ContainerInterface $container
     * @return ARequest
     */
    public static function getInstance(ContainerInterface $container): ARequest
    {
        $class = static::class;
        if (!isset(self::$instance[$class])){
            self::$instance[$class] = new $class($container);
        }
        return self::$instance[$class];
    }

    /**
     * @return Logger
     */
    public function getLogger(): Logger
    {
        return $this->logger;
    }

    /**
     * @param array $collection
     */
    public function resolve(array &$collection): void
    {
        foreach($collection as &$item) {
            foreach($item as $index => &$value) {
                if (is_array($value) && count($value)) {
                    try {
                        if ($index === 'content') {
                            $contents = new Client('tasks.api_contents', 8080);
                            $contents->write(['command' => 'collection', 'arguments' => $value]);
                            $response = $contents->read();
                            $value = $response['message'];
                        } else if ($index === 'category') {
                            $categories = new Client('tasks.api_categories', 8080);
                            $categories->write(['command' => 'collection', 'arguments' => $value]);
                            $response = $categories->read();
                            $value = $response['message'];
                        } else if ($index === 'product') {
                            $products = new Client('tasks.api_products', 8080);
                            $products->write(['command' => 'collection', 'arguments' => $value]);
                            $response = $products->read();
                            $value = $response['message'];
                        }
                    } catch (SocketException | SocketMessageErrorException $e) {
                        $this->logger->debug('RESOLVE_ERROR', [
                            'message' => $e->getMessage(),
                            'code' => $e->getCode(),
                            'file' => $e->getFile(),
                            'line' => $e->getLine()
                        ]);
                    }
                }
            }
        }
    }

}